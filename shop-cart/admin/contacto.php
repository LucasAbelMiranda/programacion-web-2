<?php include('header.php'); ?>

<h2 class="text-center">Contacto</h2>


<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">name</th>
            <th scope="col">email</th>
            <th scope="col">tel</th>
            <th scope="col">area</th>
            <th scope="col">Mensaje</th>
            <th scope="col">Accion</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $datos = file_get_contents('./../data/contacto.json');
        $datosJson = json_decode($datos, true);
        $i = -1;
        foreach ($datosJson as $cat) { 
            $i+=1;?>
            <tr>
                <th scope="row"><?php echo $i ?></th>
                <td><?php echo $cat['id'] ?></td>
                <td><?php echo $cat['name'] ?></td>
                <td><?php echo $cat['email']?></td>
                <td><?php echo $cat['tel'] ?></td>
                <td><?php echo $cat['area']?></td>
                <td><?php echo $cat['mensaje']?></td>
                <td><a href="comentario_add.php?delc=<?php echo $cat['id'] ?>"><i class="fa fa-fw fa-trash  fa-2x"></i></td>
            </tr>
    </tbody>
<?php } ?>
</table>
<?php include_once('footer.php'); ?>