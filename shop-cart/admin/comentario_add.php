<?php
//include('header.php');
include('funcs.php');

//obtengo el contenido del archivo
$datos = file_get_contents('./../data/comentarios.json');
//convierto a un array
$datosJson = json_decode($datos, true);

if (isset($_POST['add'])) {
    if (isset($_GET['edit'])) {
        $id = $_GET['edit'];
    } else {
        $id = date('Ymdhis');
    }
    // $datosJson[$id] = array('id'=>$id, 'nombre'=>$_POST['nombre']);
    $datosJson[$id] = array('id' => $id, 'score' => $_POST['score'], 'idProducto' => $_POST['id'], 'desc' => $_POST['desc']);
    //trunco el archivo
    $fp = fopen('./../data/comentarios.json', 'w');
    //convierto a json string
    $datosString = json_encode($datosJson);
    //guardo el archivo
    fwrite($fp, $datosString);
    fclose($fp);

    //redirect('./../index.php');
    echo "<script>
alert('Gracias por dejarnos un comentario!');
window.location.href='./../index.php';
</script>";
}


if (isset($_POST['cont'])) {
    $datos = file_get_contents('./../data/contacto.json');
    //convierto a un array
    $datosJson = json_decode($datos, true);
    if (isset($_GET['edit'])) {
        $id = $_GET['edit'];
    } else {
        $id = date('Ymdhis');
    }
    // $datosJson[$id] = array('id'=>$id, 'nombre'=>$_POST['nombre']);
    $datosJson[$id] = array(
        'id' => $id, 'name' => $_POST['name'], 'email' => $_POST['email'], 'tel' => $_POST['tel'],
        'area' => $_POST['area'], 'mensaje' => $_POST['mensaje']
    );
    //trunco el archivo
    $fp = fopen('./../data/contacto.json', 'w');
    //convierto a json string
    $datosString = json_encode($datosJson);
    //guardo el archivo
    fwrite($fp, $datosString);
    fclose($fp);



    //redirect('./../index.php');
    echo "<script>
alert('Gracias por contactarse con nosotros, en breve nos estaremos comunicando con usted.');
window.location.href='./../index.php';
</script>";
}


if (isset($_GET['del'])) {
    //obtengo el contenido del archivo
    $datos = file_get_contents('./../data/comentarios.json');
    //convierto a un array
    $datosJson = json_decode($datos, true);
    //var_dump($datosJson);
    //borro del array
    unset($datosJson[$_GET['del']]);
    //trunco el archivo
    $fp = fopen('./../data/comentarios.json', 'w');
    //convierto a json string
    $datosString = json_encode($datosJson);
    //guardo el archivo
    fwrite($fp, $datosString);
    fclose($fp);
    redirect('comentarios.php');
}


if (isset($_GET['delc'])) {
    //obtengo el contenido del archivo
    $datos = file_get_contents('./../data/contacto.json');
    //convierto a un array
    $datosJson = json_decode($datos, true);
    //var_dump($datosJson);
    //borro del array
    unset($datosJson[$_GET['delc']]);
    //trunco el archivo
    $fp = fopen('./../data/contacto.json', 'w');
    //convierto a json string
    $datosString = json_encode($datosJson);
    //guardo el archivo
    fwrite($fp, $datosString);
    fclose($fp);
    redirect('contacto.php');
}

if (isset($_GET['edit'])) {
    $dato = $datosJson[$_GET['edit']];
}
?>

<?php
?>
<?php include('footer.php') ?>