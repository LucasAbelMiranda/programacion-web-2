<?php include('header.php');

//obtengo el contenido del archivo
$datos = file_get_contents('./../data/productos.json');
//convierto a un array
$datosJson = json_decode($datos, true);

if (isset($_POST['add'])) {
    if (isset($_GET['edit'])) {
        $id = $_GET['edit'];
    } else {
        $id = date('Ymdhis');
    }

    // 1 => array("id" => 1, "nombre" => "producto1", "precio" => "22.0","categoria"=> "1", "imagen"=> "d.jpg","marca" => 1,"desc"=>"Nowadays the lingerie industry is one of the most successful business spheres. We always stay in touch with the latest fashion tendencies - that is why our goods are so popular."),

    $datosJson[$id] = array(
        'id' => $id,
        'nombre' => $_POST['nombre'],
        'precio' => $_POST['precio'],
        'categoria' => $_POST['categoria'],
        'imagen' => $_POST['imagen'],
        'marca' => $_POST['marca'],
        'habilitado' => ($_POST['habilitado']=='on') ? 'true':'false',
        'desc' => $_POST['desc']
    );



    //trunco el archivo
    $fp = fopen('./../data/productos.json', 'w');
    //convierto a json string
    $datosString = json_encode($datosJson);
    //guardo el archivo
    fwrite($fp, $datosString);
    fclose($fp);
    redirect('producto.php');
}


if (isset($_GET['del'])) {
    //obtengo el contenido del archivo
    $datos = file_get_contents('./../data/productos.json');
    //convierto a un array
    $datosJson = json_decode($datos, true);
    //var_dump($datosJson);
    //borro del array
    unset($datosJson[$_GET['del']]);
    //trunco el archivo
    $fp = fopen('./../data/productos.json', 'w');
    //convierto a json string
    $datosString = json_encode($datosJson);
    //guardo el archivo
    fwrite($fp, $datosString);
    fclose($fp);
    redirect('producto.php');

}

if (isset($_GET['edit'])) {
    $dato = $datosJson[$_GET['edit']];
    /*     var_dump($_GET['edit']);
    var_dump($dato['marca']); */
    //var_dump($dato['habilitado']);
}
else{
    $dato=null;
}

?>
<h1 class="text-center m-10">Añadir Producto</h1>
<div class="mt-1 d-flex align-items-center justify-content-center text-center">
    <div class="row">
        <form action="" method="post">

            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input name="nombre" id="nombre" placeholder="nombre del producto" value="<?php echo isset($dato) ? $dato['nombre'] : '' ?>">
            </div>
            <div class="form-group">
                <label for="categoria">Categoria</label>
                <select class="browser-default custom-select" name="categoria">
                    <?php
                    $datos = file_get_contents('./../data/categorias.json');
                    $datosJson = json_decode($datos, true);
                    foreach ($datosJson as $cat) { ?>
                        <option <?php echo ($dato['categoria'] == $cat['nombre']) ? "selected" : "" ?>> <?php echo $cat['nombre'] ?></option>
                    <?php } ?>

                </select>

            </div>

            <div class="form-group">
                <label for="precio">Precio</label>
                <input name="precio" id="precio" placeholder="$" value="<?php echo isset($dato) ? $dato['precio'] : '' ?>">
            </div>
            <div class="form-group">
                <label for="categoria">Imagen</label>
                <select class="browser-default custom-select" name="imagen">
                    <?php
                    $datos = file_get_contents('./../data/imagenes.json');
                    $datosJson = json_decode($datos, true);
                    foreach ($datosJson as $cat) { ?>
                        <option <?php echo ($dato['imagen'] == $cat) ? "selected" : "" ?>> <?php echo $cat ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="marca">Marca</label>
                <!--  <input name="categoria" id="categoria" placeholder="seleccionar" value="<?php echo isset($dato) ? $dato['marca'] : '' ?>"> -->
                <select class="browser-default custom-select" name="marca">
                    <?php
                    $datos = file_get_contents('./../data/marcas.json');
                    $datosJson = json_decode($datos, true);
                    foreach ($datosJson as $cat) { ?>
                        <option <?php echo ($dato['marca'] == $cat['nombre']) ? "selected" : "" ?>> <?php echo $cat['nombre'] ?></option>
                    <?php } ?>
                </select>
                <div class="form-group">
                    <label for="desc">Descripcion del producto</label>
                    <textarea class=" form-control" rows="5" id="desc" name="desc"><?php echo isset($dato) ? $dato['desc'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="habilitado" name="habilitado" <?php echo($dato['habilitado']=='true') ? "checked":'' ?> >
                <label class="form-check-label" for="habilitado" >
                    habilitar producto
                </label>
            </div>
            <br>
            <button type="submit" name="add" class="btn btn-primary">Aceptar</button>
        </form>
    </div>
</div>


<?php include('footer.php') ?>