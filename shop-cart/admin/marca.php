<?php include('header.php');?>

<h2 class="text-center">Marcas</h2>
<a href="marca_add.php"><i class="fa fa-plus-circle  fa-4x" aria-hidden="true"></i></a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">NOMBRE</th>
            <th scope="col">ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $datos = file_get_contents('./../data/marcas.json');
        $datosJson = json_decode($datos, true);
        $i=-1;
        foreach ($datosJson as $cat ) { 
            $i+=1;
            ?>
            <tr>
                <th scope="row"><?php echo $i?></th>
                <td><?php echo $cat['id'] ?></td>
            <td><?php echo $cat['nombre'] ?></td>
                <td><a href="marca_add.php?edit=<?php echo $cat['id'] ?>"><i class="fa fa-fw fa-pencil  fa-2x"></i></a> /
                    <a href="marca_add.php?del=<?php echo $cat['id'] ?>"><i class="fa fa-fw fa-trash  fa-2x"></i></td>
            </tr>
    </tbody>
<?php } ?>
</table>
<?php include_once('footer.php'); ?>