<?php include('header.php');

?>

<?php

if (isset($_GET['del'])) {
    //obtengo el contenido del archivo
    $datos = file_get_contents('./../data/categorias.json');
    //convierto a un array
    $datosJson = json_decode($datos, true);
    //var_dump($datosJson);
    //borro del array
    unset($datosJson[$_GET['del']]);
    //trunco el archivo
    $fp = fopen('./../data/categorias.json', 'w');
    //convierto a json string
    $datosString = json_encode($datosJson);
    //guardo el archivo
    fwrite($fp, $datosString);
    fclose($fp);
    redirect('categoria.php');
}
?>

<h2 class="text-center">Categorias</h2>
<a href="categoria_add.php"><i class="fa fa-plus-circle  fa-4x" aria-hidden="true"></i></a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">NOMBRE</th>
            <th scope="col">ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $datos = file_get_contents('./../data/categorias.json');
        $datosJson = json_decode($datos, true);
        $i = -1;
        foreach ($datosJson as $cat) {
            $i++; ?>
            <tr>
                <th scope="row"><? echo $i?></th>
                <td><?php echo $cat['id'] ?></td>
                <td><?php echo $cat['nombre'] ?></td>
                <td><a href="categoria_add.php?edit=<?php echo $cat['id'] ?>"><i class="fa fa-fw fa-pencil  fa-2x"></i></a> /
                    <a href="categoria.php?del=<?php echo $cat['id'] ?>"><i class="fa fa-fw fa-trash  fa-2x"></i></td>
            </tr>
    </tbody>
<?php } ?>
</table>


<?php include_once('footer.php'); ?>