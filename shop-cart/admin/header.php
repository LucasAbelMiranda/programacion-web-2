<?php session_start();

include('funcs.php');

if (isset($_POST['login'])) {
    if ($_POST['pass'] == '123456' && $_POST['user'] == 'admin') {
        $_SESSION['usuario_logueado'] = true;
    }
}

if (isset($_GET['logout'])) {
    unset($_SESSION['usuario_logueado']);
}

if (!isset($_SESSION['usuario_logueado'])) {
    redirect('login.php');
}


?>

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<div class="container ">
    <div class="row">
        <div class="col-8">
        </div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php"><i class="fa fa-fw fa-home"></i></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
            <a class="nav-item nav-link" href="producto.php">Producto</a>      
                <a class="nav-item nav-link" href="comentarios.php">Comentarios</a>
                <a class="nav-item nav-link" href="contacto.php">Contacto</a>
                <a class="nav-item nav-link " href="categoria.php">Categoria</a></a>  
                <a class="nav-item nav-link" href="marca.php">marca</a>
                <a class="nav-item nav-link" href="imagenes.php">imagenes</a>
                <a class="nav-item nav-link" href="index.php?logout">Logout</a>
            </div>
        </div>
    </nav>
</div>