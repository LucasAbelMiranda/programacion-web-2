<?php include('header.php'); ?>

<form action="upload.php" method="post" enctype="multipart/form-data">
  Select image to upload:
  <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="submit" value="Upload Image" name="submit">
</form>
<div class="row image-grid">

    <?php
    $datos = file_get_contents('./../data/imagenes.json');
    $datosJson = json_decode($datos, true);

    foreach ($datosJson as $cat) { ?>
    <div class="col-sm-4 col-md-2 ">
        <div class="panel panel-default">
            <div class="panel-body"><a href="./../assets/productos/<?php echo $cat?>" target="_blank"><img alt="" class="img-responsive center-block"  height="100" src="./../assets/productos/<?php echo $cat?>" /></a></div>
            <div class="panel-footer"><a href="upload.php?del=<?php echo $cat ?>"><i class="fa fa-fw fa-trash" aria-hidden="true"></i></a></div>
            <?php echo $cat ?> <?php echo $cat ?>
           
        </div>
</div>

<?php } ?>


<?php include_once('footer.php'); ?>


