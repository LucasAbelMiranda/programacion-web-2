<?php include('header.php');
 
 

//obtengo el contenido del archivo
$datos = file_get_contents('./../data/categorias.json');
//convierto a un array
$datosJson = json_decode($datos,true);

    if(isset($_POST['add'])){
        if(isset($_GET['edit'])){
            $id = $_GET['edit'];
        }else{
            $id = date('Ymdhis');
        }

        $datosJson[$id] = array('id'=>$id, 'nombre'=>$_POST['nombre']);
    
        //trunco el archivo
        $fp = fopen('./../data/categorias.json','w');
        //convierto a json string
        $datosString = json_encode($datosJson);
        //guardo el archivo
        fwrite($fp,$datosString);
        fclose($fp);
        redirect('categoria.php');
    }

    if(isset($_GET['edit'])){
        $dato = $datosJson[$_GET['edit']];
    }
?>


<div class="d-flex align-items-center justify-content-center text-center" style="height: 350px">
    <div class="row">
        <form action="" method="post">
            <div class="form-group">
                <label for="nombre">Producto</label>
                <input name="nombre" id="nombre" placeholder="nombre" value="<?php echo isset($dato)?$dato['nombre']:''?>">
            </div>
            <button type="submit" name="add" class="btn btn-primary">Aceptar</button>
        </form>
    </div>
</div>


<?php include('footer.php')?>