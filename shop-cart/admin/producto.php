<?php include('header.php'); ?>


<h2 class="text-center">productos</h2>
<a href="producto_add.php"><i class="fa fa-plus-circle  fa-4x" aria-hidden="true"></i></a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Categoria</th>
            <th scope="col">Precio</th>
            <th scope="col">imagen</th>
            <th scope="col">marca</th>
            <th scope="col">habilitado</th>
            <th scope="col">desc</th>
            <th scope="col">ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $datos = file_get_contents('./../data/productos.json');
        $datosJson = json_decode($datos, true);
        $i = -1;
        foreach ($datosJson as $cat) { 
            $i+=1; ?>
            <tr>
                <th scope="row"><?php echo $i?></th>
                <td><?php echo $cat['id'] ?></td>
                <td><?php echo $cat['nombre'] ?></td>
                <td><?php echo $cat['categoria'] ?></td>
                <td><?php echo $cat['precio'] ?></td>
                <td><div class="panel-body"><a href="./../assets/productos/<?php echo $cat['imagen']?>" target="_blank"><img alt="" class="img-responsive center-block"  height="100" src="./../assets/productos/<?php echo $cat['imagen']?>" /></a></div>
                    <?php echo $cat['imagen'] ?></td>
                <td><?php echo $cat['marca'] ?></td>
                <td><input type="checkbox" name="habilitado" disabled <?php echo($cat['habilitado']=='true') ? "checked":'' ?> value=""></td>
                <td><?php echo $cat['desc'] ?></td>
                <td><a href="producto_add.php?edit=<?php echo $cat['id'] ?>"><i class="fa fa-fw fa-pencil  fa-2x"></i></a> /
                    <a href="producto_add.php?del=<?php echo $cat['id'] ?>"><i class="fa fa-fw fa-trash  fa-2x"></i></td>
            </tr>
    </tbody>
<?php } ?>
</table>
<?php include_once('footer.php'); ?>


