<?php include('header.php'); ?>

<h2 class="text-center">comentarios</h2>


<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">ID producto</th>
            <th scope="col">Score</th>
            <th scope="col">Comentario</th>
            <th scope="col">Accion</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $datos = file_get_contents('./../data/comentarios.json');
        $datosJson = json_decode($datos, true);
        $i = -1;
        foreach ($datosJson as $cat) {
            $i += 1; ?>
            <tr>
                <th scope="row"><?php echo $i ?></th>
                <td><?php echo $cat['id'] ?></td>
                <td><?php echo $cat['idProducto'] ?></td>
                <td><?php echo $cat['score'] ?></td>
                <td><?php echo $cat['desc'] ?></td>
                <td><a href="comentario_add.php?del=<?php echo $cat['id'] ?>"><i class="fa fa-fw fa-trash  fa-2x"></i></td>
            </tr>
    </tbody>
<?php  } ?>
</table>
<?php include_once('footer.php'); ?>