<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Twitter Bootstrap shopping cart</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Bootstrap styles -->
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
	<!-- Customize styles -->
	<link href="style.css" rel="stylesheet" />
	<!-- font awesome styles -->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
	<!--[if IE 7]>
			<link href="css/font-awesome-ie7.min.css" rel="stylesheet">
		<![endif]-->

	<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	<!-- Favicons -->
	<link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
	<!-- 
	Upper Header Section 
-->
	<?php
	include_once('shared/header.php')
	?>
	<!-- 
Body Section 
-->
	<hr class="soften">
	<div class="well well-small">
		<h1>Visit us</h1>
		<hr class="soften" />
		<div class="row-fluid">
			<div class="span8 relative">
				<iframe style="width:100%; height:350px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.co.uk/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Newbury+Street,+Boston,+MA,+United+States&amp;aq=1&amp;oq=NewBoston,+MA,+United+States&amp;sll=42.347238,-71.084011&amp;sspn=0.014099,0.033023&amp;ie=UTF8&amp;hq=Newbury+Street,+Boston,+MA,+United+States&amp;t=m&amp;ll=42.348994,-71.088248&amp;spn=0.001388,0.006276&amp;z=18&amp;iwloc=A&amp;output=embed"></iframe>

				<div class="absoluteBlk">
					<div class="well wellsmall">
						<h4>Contact Details</h4>
						<h5>
							2601 Mission St.<br />
							San Francisco, CA 94110<br /><br />

							info@mysite.com<br />
							﻿Tel 123-456-6780<br />
							Fax 123-456-5679<br />
							web:wwwmysitedomain.com
						</h5>
					</div>
				</div>
			</div>

			<div class="span4">
				<h4>Email Us</h4>
				<form class="form-horizontal" action="admin/comentario_add.php" method="post">
					<fieldset>

						<div class="control-group">

							<input type="text" name="name" placeholder="nombre y apellido" class="input-xlarge" required/>

						</div>
						<div class="control-group">

							<input type="text" name="email" type="email" placeholder="email" class="input-xlarge" required />

						</div>
						<div class="control-group">

							<input type="text" name="tel" placeholder="telefono" class="input-xlarge" required/>

						</div>
						<div class="control-group">

							<input type="text" name="area" placeholder=" Área de la empresa a quien se quiere mandar la consulta." class="input-xlarge" required />

						</div>
						<div class="control-group">
							<textarea rows="3" id="textarea" name="mensaje" class="input-xlarge" required></textarea>

						</div>

						<button class="shopBtn" name="cont" type="submit">enviar</button>

					</fieldset>
				</form>
			</div>
		</div>


	</div>
	<!-- 
Clients 
-->


	<!--
Footer
-->
	
	<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
	<script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
	<script src="assets/js/shop.js"></script>
</body>

</html>