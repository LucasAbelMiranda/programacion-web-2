<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Carrito de compra</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<link href="assets/css/bootstrap.css" rel="stylesheet" />
	<!-- Customize styles -->
	<link href="style.css" rel="stylesheet" />
	<!-- font awesome styles -->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">

	<!-- Favicons -->
	<link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
	<!-- Header -->
	<?php
	require_once('shared/header.php');
	?>
	<!-- Body Section -->
	<div class="row">
		<?php include_once('shared/sidebar.php') ?>
		<div class="span9">
			<div class="well np">
				<!--?php 
	require_once('shared/carousel.php'); //para uso futuro
?-->
				<!--
New Products
-->
				<!--Featured Products-->
				<div class="well well-small">
					<h3><a class="btn btn-mini pull-right" href="products.php" title="View more">Ver mas productos<span class="icon-plus"></span></a> Featured Products </h3>
					<hr class="soften" />
					<div class="row-fluid">
						<ul class="thumbnails">
							<?php
							include('data/productos.php');
							foreach ($productos as $elemento) {
								if ($elemento['habilitado'] == 'true') {
							?>
									<li class="">
										<div class="thumbnail">
											<a class="zoomTool" href="product_details.php?id=<?php echo $elemento['id'] ?>" title="add to cart"><span class="icon-search"></span> vista rapida</a>
											<a href="product_details.php?id=<?php echo $elemento['id'] ?>"><img src="assets/productos/<?php echo $elemento['imagen'] ?>" alt=""></a>
											<div class="caption">
												<h5><?php echo $elemento['nombre'] ?></h5>
												<h4>
													<a class="defaultBtn" href="product_details.php?id=<?php echo $elemento['id'] ?>" title="Click to view"><span class="icon-zoom-in"></span></a>
													<a class="shopBtn" href="#" title="add to cart"><span class="icon-plus"></span></a>
													<span class="pull-right">$<?php echo $elemento['precio'] ?></span>
												</h4>
											</div>
										</div>
									</li>
							<?php }
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!--Footer-->
		<?php include_once('shared/footer.php')	?>
	</div>

	<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
	<script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
	<script src="assets/js/shop.js"></script>
</body>

</html>