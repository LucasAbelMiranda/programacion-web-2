<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Twitter Bootstrap shopping cart</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Bootstrap styles -->
	<link href="assets/css/bootstrap.css" rel="stylesheet" />
	<!-- Customize styles -->
	<link href="style.css" rel="stylesheet" />
	<!-- font awesome styles -->
	<link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
	<!--[if IE 7]>
			<link href="css/font-awesome-ie7.min.css" rel="stylesheet">
		<![endif]-->

	<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

	<!-- Favicons -->
	<link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>


	<?php include_once('shared/header.php') ?>

	<div class="row">
		<?php include_once('shared/sidebar.php') ?>
		<div class="span9">
			<!-- 
New Products
-->
			<div class="well well-small">
				<h3>Our Products </h3>
				<div class="row-fluid">
					<ul class="thumbnails">
						<?php
						include('data/productos.php');
						foreach ($productos as $prod) {
							$imprimir = true;
							if (!empty($_GET['cat'])) {
								if ($prod['categoria'] != $_GET['cat']) {
									$imprimir = false;
								}
							}
							if ($imprimir && !empty($_GET['marca'])) {
								if ($prod['marca'] != $_GET['marca']) {
									$imprimir = false;
								}
							}
							if($prod['habilitado']=='false'){
								$imprimir=false;
							}


							if ($imprimir) {
						?>
								<li class="span4">
									<div class="thumbnail">
										<a href="product_details.php?id=<?php echo $prod['id']?>" class="overlay"></a>
										<a class="zoomTool" href="product_details.php?id=<?php echo $prod['id']?>" title="add to cart"><span class="icon-search"></span> vista rapida</a>
										<a href="product_details.php?id=<?php echo $prod['id']?>"><img src="assets/productos/<?php echo $prod["imagen"] ?>" alt=""></a>
										<div class="caption cntr">
											<p><?php echo $prod["nombre"] ?></p>
											<p><strong> $<?php echo $prod["precio"] ?></strong></p>
											<h4><a class="shopBtn" href="#" title="add to cart"> Add to cart </a></h4>
											<div class="actionList">
												<a class="pull-left" href="#">Add to Wish List </a>
												<a class="pull-left" href="#"> Add to Compare </a>
											</div>
											<br class="clr">
										</div>
									</div>
								</li>
						<?php }
						}
						?>
					</ul>
				</div>

			</div>
		</div>
	</div>
	<!-- 
Clients 
-->
	<?php include_once('shared/footer.php') ?>
	<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
	<script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
	<script src="assets/js/shop.js"></script>
</body>

</html>