<?php include_once('shared/header.php'); ?>
<!-- 
Body Section 
-->
<div class="row">
	<?php include_once('shared/sidebar.php'); ?>
	<div class="span9">
		<ul class="breadcrumb">
			<li><a href="index.php">Home</a> <span class="divider">/</span></li>
			<li><a href="products.php">Items</a> <span class="divider">/</span></li>
			<li class="active">Preview</li>
		</ul>
		<div class="well well-small">
			<div class="row-fluid">
				<div class="span5">
					<?php include('data/productos.php');
					$productoSeleccionado = null;
					foreach ($productos as $elemento) {
						if ($elemento['id'] == $_GET['id'])
							$productoSeleccionado = $elemento;
					}
					?>
					<img src="assets/productos/<?php echo $productoSeleccionado['imagen'] ?>" alt="">
				</div>
				<div class="span7">
					<h3>Precio $<?php echo $productoSeleccionado['precio'] ?></h3>
					<hr class="soft" />
					<form class="form-horizontal qtyFrm">
						<div class="control-group">
						</div>
						<h4>item in stock</h4>
						<!-- <button type="submit" class="shopBtn"><span class=" icon-shopping-cart"></span>Añadir al carrito</button> -->
					</form>
				</div>
				<div>
					<p>
						<?php echo $productoSeleccionado['desc'] ?>
					</p>
				</div>
			</div>
			<hr class="softn clr" />
			<form action="admin/comentario_add.php" method="post">
				<input class="form-control" type="text" name='id' value="<?php echo $productoSeleccionado['id'] ?>" placeholder="<?php echo $productoSeleccionado['id'] ?>" readonly>
				<div class="form-group">
					<label for="desc"></label>
					<input class="form-control" required type="text" id="desc" name='desc' placeholder="Dejanos un comentario">
				</div>
				<label for="example-number-input" class="col-2 col-form-label">puntaje</label>
				<div class="col-10">
					<i style="font-size: 20px; color: rgb(195, 207, 112);" class="fa fa-star"></i>
					<input class="form-control" type="number" name='score' value="5" min="1" max="5" id="example-number-input">
				</div>

				<button type="submit" name="add" class="btn btn-primary">Enviar comentario</button>
			</form>
			<div class="">
				<h2>Comentarios</h2>
				<?php
				$datos = file_get_contents('data/comentarios.json');
				$datosJson = json_decode($datos, true);
				$reversed = array_reverse($datosJson);
				$reversed = array_slice($reversed, 0, 10);
				$i = -1;
				foreach ($reversed as $cat) {
					$i += 1;
					if ($cat['idProducto'] == $_GET['id']) {
				?>
						<div class="col-md-4">
							<?php for ($i = 1; $i <= $cat['score']; $i++) { ?>
								<i style="font-size: 20px; color: rgb(195, 207, 112);" class="fa fa-star"></i>
							<?php } ?>
							<p><?php echo $cat['desc'] ?></p>
							</br>
						</div>
				<?php  }
				} ?>

			</div>
		</div>
	</div>



</div>
</div>
</div> <!-- Body wrapper -->
<?php include_once('shared/footer.php'); ?>
</footer>
</div><!-- /container -->


<a href="#" class="gotop"><i class="icon-double-angle-up"></i></a>
<!-- Placed at the end of the document so the pages load faster -->
<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.easing-1.3.min.js"></script>
<script src="assets/js/jquery.scrollTo-1.4.3.1-min.js"></script>
<script src="assets/js/shop.js"></script>
</body>

</html>