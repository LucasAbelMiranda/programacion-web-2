<div id="sidebar" class="span3">
	<div class="well well-small">
		<ul class="nav nav-list">
			<h3>Categorias</h3>
			<?php

			include('data/categorias.php');
			foreach ($categorias as $cat) {
			?>
				<li><a href="products.php?cat=<?php echo $cat['nombre'] ?>&marca=<?php echo isset($_GET['marca']) ? $_GET['marca'] : '' ?>"><span class="icon-chevron-right"></span><?php echo $cat['nombre'] ?></a></li>
			<?php } ?>
			<li><a href="products.php?marca=<?php echo isset($_GET['marca']) ? $_GET['marca'] : '' ?>""><span class="icon-chevron-right"></span>ver todos</a></li>
			<!--esta borrando marcas-->
			<li style="border:0"> &nbsp;</li>
			<!-- <li> <a class="totalInCart" href="cart.php"><strong>Total Amount <span class="badge badge-warning pull-right" style="line-height:18px;">$448.42</span></strong></a></li> -->
		</ul>
	</div>
	<div class="well well-small">
		<h3>Marcas</h3>
		<ul class="nav nav-list">
			<?php
			include_once('data/marcas.php');
			foreach ($marcas as $m) {
			?>
				<li><a href="products.php?marca=<?php echo $m['nombre'] ?>&cat=<?php echo isset($_GET['cat']) ? $_GET['cat'] : '' ?>"><span class="icon-chevron-right"></span><?php echo $m['nombre'] ?></a></li>
			<?php } ?>
			<li><a href="products.php?cat=<?php echo isset($_GET['cat']) ? $_GET['cat'] : '' ?>"><span class="icon-chevron-right"></span>Todos</a></li>
		</ul> <!-- revisar -->
	</div>
</div>